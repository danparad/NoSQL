/*
We wish to perform a case insensitive query searching for an address that
containd the word university. Since the location of the work in the address
does not matter we use a '%' symbol before and after the word to look for it 
in the street_address field.
*/
SELECT name
FROM venues
WHERE street_address ILIKE '%university%';
