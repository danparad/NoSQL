// The following package is required to run my script :
// npm install --save mongoose-sequence

var mongoose = require('mongoose');

const AutoIncrement = require('mongoose-sequence')(mongoose);

mongoose.connect('mongodb://localhost/Pizzeria');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
	
   //Print message to the console so we know it's working
  console.log('Got this working !');
  
//  Orders
//   We create a variable which contains the schema, in which we constraint it to contain the following fields :
  var orders = new mongoose.Schema({
  	order_id: {type: Number, require: true},
	user_id: {type: Number, required: true},
  	date: {type: Date, default: Date.now},
  	recipe: {type: String, required: true}
  });
//  This automatically increments the id field in users (which doesn't have 
//  to be declared explicitly because of this AutoIncrement function)
  orders.plugin(AutoIncrement,{inc_field: 'order_id'});
  
  
  // Users
  // We create a variable which contains the schema, in which we constraint it to contain the following fields :
  var users = new mongoose.Schema({
  	id: {type: Number, unique: true},
    first_name: {type: String, required: true},
    last_name: {type: String, required: true},
    email_address: String,
    phone_number: String,
    address_line1: String,
    address_line2: String,
    city: String,
    state: String,
    zip: Number
  });
  
  
  //Inventory
  // We create a variable which contains the schema, in which we constraint it to contain the following fields :
  var inventory = new mongoose.Schema({
  	name: {type: String, required: true, unique: true},
  	description: String,
    quantity: {type: Number, required: true, min: 0, default: 50000} //, default: 50000
  });


  //Recipes
  // We create a variable which contains the schema, in which we constraint it to contain the following fields :
  var recipes = new mongoose.Schema({
  	name: {type: String, required: true, unique: true},
  	description: String,
  	ingredients: {
  		ingredient_1: {type: String, required: true, quantity: Number},
  		ingredient_2: {type: String, quantity: Number},
  		ingredient_3: {type: String, quantity: Number},
  		ingredient_4: {type: String, quantity: Number},
  		ingredient_5: {type: String, quantity: Number},
  		ingredient_6: {type: String, quantity: Number},
  		},
  	instructions: String
  });
  
  
  //Create the models :
//  var shit = mongoose.model('shit',shit);
  var orders = mongoose.model('orders', orders);
  var users = mongoose.model('users',users);
  var inventories = mongoose.model('inventories',inventory);
  var recipes = mongoose.model('recipes',recipes);


//This bit of fun code was meant to be a post middleware, meaning that this was a function that would be added to the orders schema as a post (i.e. function executed after the 'hooked' method, in this case an insert on the orders collection and its pre middleware (functions perfomed before a given action)). However I ran into some walls that I didn't know how to circumvent or bring down. 
//The idea behind this was to do a post insert operation where for each entry into the orders data base we would retrive the max order_id number (which would be the latest one to have been inserted) and find the document in the recipes collection and retrieve the ingredients. Then perform for each subdocument and update on the quantities of the corresponding ingredient in the iventories collection (which afterwards was going to check if any of the quantities were below our threshold and raise a warning and reinsert the ingredients in the inventory collection)

//orders.post('insert',
//function(orders.find({$max:{order_id}}{recipe:1}){
//	recipes.find({"recipe_name":orders.find({$max:{order_id}},{recipe_name:1})},{ingredients:1}).foreach(
//	function(ing){
//		inventories.update({"ingredient":ing},{$inc: {"quantity":-1}
//		})) 
//	} 
//  ) 
//); 
  
  console.log('Got something');
});
