# Author : Daniel Parada

# We import the packages we need
import random
import os

r = random.SystemRandom()

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/neo4j-community-3.4.5/import/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'treatments_patients.csv', 'w')

# We write the command to create the doctor nodes :
outputfile_1.write("treatment_id,patient_id\n")

# We loop over 10000 cases to treat all of our sick patients :
for i in range(1, 10001):
    treatment = random.sample(range(1, 750), 5)
    for j in range(r.randint(1, 5)):
        outputfile_1.write(str(treatment[j]) + "," + str(i) + "\n")

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open(outputpath + 'treatments_patients.csv', 'rb+') as filehandle:
    filehandle.seek(-1, os.SEEK_END)
    filehandle.truncate()


















