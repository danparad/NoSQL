--Populate users table:
\i users.sql

--Populate inventory table
\i insert_ingredients.sql

--Populate recipes table
\i recipes.sql

--Populate orders table
\i orders.sql

--Populating table ingredients_recipes
INSERT INTO ingredients_recipes (recipe_name, ingredient_name) ( 
	SELECT recipes.name, inventory.name 
	FROM recipes JOIN inventory 
	ON inventory.name IN (recipes.ingredient_1,recipes.ingredient_2,recipes.ingredient_3,recipes.ingredient_4,recipes.ingredient_5,recipes.ingredient_6 ));