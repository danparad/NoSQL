CREATE OR REPLACE FUNCTION place_order(
  user_id INT,
  pizza text,
  size INT DEFAULT 1)
  RETURNS boolean AS $$
  
BEGIN

--Insert the order into the orders table
  INSERT INTO orders (user_id, recipe_name)
  VALUES (user_id, pizza);

--Update the inventory removing the ingredients for the pizza
  UPDATE inventory
  SET quantity = quantity - goodies.qty*size
  FROM (SELECT name, qty
        FROM inventory JOIN ingredients_recipes
        ON inventory.name = ingredients_recipes.ingredient_name
        WHERE recipe_name = pizza) AS goodies
  WHERE inventory.name = goodies.name;

--If any of the ingredients for the pizza are below 0, it means we ran
--out and can't make the pizza
  IF (
  SELECT COUNT(*) 
  FROM inventory 
  WHERE inventory.quantity < 0) > 0 THEN
    UPDATE orders
    SET order_placed = FALSE
    WHERE order_id = (
    			SELECT order_id 
    			FROM orders 
    			ORDER BY order_id DESC LIMIT 1);
    --Raise a notice signifying we don't have the goods for the pizza
    RAISE NOTICE 'Ran out of the good stuff';

--So we add the ingredients back in and change the status of the order
--to false    
	UPDATE inventory
    SET quantity = quantity + good_stuff.qty*size
    FROM (SELECT name, qty
          FROM inventory JOIN ingredients_recipes
          ON inventory.name = ingredients_recipes.ingredient_name
          WHERE recipe_name = pizza) AS good_stuff
    WHERE inventory.name = good_stuff.name;

    END IF;

  RETURN TRUE;

END;

$$
LANGUAGE plpgsql;






