Built-in reduce functions are implemented in Erlang and run inside of CouchDB, making them much faster than their equivalent JavaScript functions.

_sum : adds up the emitted values (numeric)

_count : counts the number of emitted values

_stats : calculates numerical statistics (sum, count, min, max,sumsqr) (numeric)