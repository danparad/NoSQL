//The following script was meant to connect to the database Pizzeria created in my other scripts and performing a reverse order query on the recipe_name field and returning the id field. I was unsuccessful in running this script however and was unable to figure out the isse. That being said, the idea behind this script was very similar to that of what we did in Postgres, run a query against a non-indexed field and check the performance against the same query but this time with an index.


var mongoose = require('mongoose');
const incr = require('mongoose-sequence')(mongoose);
mongoose.connect('mongodb://localhost/Pizzeria');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    
    var noIndexQuery = orders.find().sort({recipe: true}).select({recipe:-1}).explain("executionStats");
    noIndexQuery.exec(function (err, docs) {
        if (err) return handleError(err);
        }
        )
        
    orders.index({recipe: 1}, {unique: true});
    var indexedQuery = orders.find().sort({recipe: true}).select({recipe: -1}).explain("executionStats");
    indexedQuery.exec(function (err, docs) {
        if (err) return handleError(err);
        }
        )
        
});



//This next portion, I ran the queries using MongoDB directly. The query looked for all of the recipes that were between 'Clark' and 'Thompson' forcing the algorithm to look at every entry. The first command was done with no index giving the folling results :

db.orders.find({recipe:{$gte: 'Clark', $lte: 'Thompson'} } ).explain("executionStats")
{
	"queryPlanner" : {
		"plannerVersion" : 1,
		"namespace" : "Pizzeria.orders",
		"indexFilterSet" : false,
		"parsedQuery" : {
			"$and" : [
				{
					"recipe" : {
						"$lte" : "Thompson"
					}
				},
				{
					"recipe" : {
						"$gte" : "Clark"
					}
				}
			]
		},
		"winningPlan" : {
			"stage" : "COLLSCAN",
			"filter" : {
				"$and" : [
					{
						"recipe" : {
							"$lte" : "Thompson"
						}
					},
					{
						"recipe" : {
							"$gte" : "Clark"
						}
					}
				]
			},
			"direction" : "forward"
		},
		"rejectedPlans" : [ ]
	},
	"executionStats" : {
		"executionSuccess" : true,
		"nReturned" : 35678,
		"executionTimeMillis" : 91,
		"totalKeysExamined" : 0,
		"totalDocsExamined" : 50000,
		"executionStages" : {
			"stage" : "COLLSCAN",
			"filter" : {
				"$and" : [
					{
						"recipe" : {
							"$lte" : "Thompson"
						}
					},
					{
						"recipe" : {
							"$gte" : "Clark"
						}
					}
				]
			},
			"nReturned" : 35678,
			"executionTimeMillisEstimate" : 88,
			"works" : 50002,
			"advanced" : 35678,
			"needTime" : 14323,
			"needYield" : 0,
			"saveState" : 391,
			"restoreState" : 391,
			"isEOF" : 1,
			"invalidates" : 0,
			"direction" : "forward",
			"docsExamined" : 50000
		}
	},
	"serverInfo" : {
		"host" : "Daniels-MacBook-Pro.local",
		"port" : 27017,
		"version" : "4.0.0",
		"gitVersion" : "3b07af3d4f471ae89e8186d33bbb1d5259597d51"
	},
	"ok" : 1
}


//This time we ran the same query with an index being applied to the field recipe in orders :


db.orders.find({recipe:{$gte: 'Clark', $lte: 'Thompson'} } ).explain("executionStats")
{
	"queryPlanner" : {
		"plannerVersion" : 1,
		"namespace" : "Pizzeria.orders",
		"indexFilterSet" : false,
		"parsedQuery" : {
			"$and" : [
				{
					"recipe" : {
						"$lte" : "Thompson"
					}
				},
				{
					"recipe" : {
						"$gte" : "Clark"
					}
				}
			]
		},
		"winningPlan" : {
			"stage" : "FETCH",
			"inputStage" : {
				"stage" : "IXSCAN",
				"keyPattern" : {
					"recipe" : 1
				},
				"indexName" : "recipe_1",
				"isMultiKey" : false,
				"multiKeyPaths" : {
					"recipe" : [ ]
				},
				"isUnique" : false,
				"isSparse" : false,
				"isPartial" : false,
				"indexVersion" : 2,
				"direction" : "forward",
				"indexBounds" : {
					"recipe" : [
						"[\"Clark\", \"Thompson\"]"
					]
				}
			}
		},
		"rejectedPlans" : [ ]
	},
	"executionStats" : {
		"executionSuccess" : true,
		"nReturned" : 35678,
		"executionTimeMillis" : 160,
		"totalKeysExamined" : 35678,
		"totalDocsExamined" : 35678,
		"executionStages" : {
			"stage" : "FETCH",
			"nReturned" : 35678,
			"executionTimeMillisEstimate" : 45,
			"works" : 35679,
			"advanced" : 35678,
			"needTime" : 0,
			"needYield" : 0,
			"saveState" : 279,
			"restoreState" : 279,
			"isEOF" : 1,
			"invalidates" : 0,
			"docsExamined" : 35678,
			"alreadyHasObj" : 0,
			"inputStage" : {
				"stage" : "IXSCAN",
				"nReturned" : 35678,
				"executionTimeMillisEstimate" : 0,
				"works" : 35679,
				"advanced" : 35678,
				"needTime" : 0,
				"needYield" : 0,
				"saveState" : 279,
				"restoreState" : 279,
				"isEOF" : 1,
				"invalidates" : 0,
				"keyPattern" : {
					"recipe" : 1
				},
				"indexName" : "recipe_1",
				"isMultiKey" : false,
				"multiKeyPaths" : {
					"recipe" : [ ]
				},
				"isUnique" : false,
				"isSparse" : false,
				"isPartial" : false,
				"indexVersion" : 2,
				"direction" : "forward",
				"indexBounds" : {
					"recipe" : [
						"[\"Clark\", \"Thompson\"]"
					]
				},
				"keysExamined" : 35678,
				"seeks" : 1,
				"dupsTested" : 0,
				"dupsDropped" : 0,
				"seenInvalidated" : 0
			}
		}
	},
	"serverInfo" : {
		"host" : "Daniels-MacBook-Pro.local",
		"port" : 27017,
		"version" : "4.0.0",
		"gitVersion" : "3b07af3d4f471ae89e8186d33bbb1d5259597d51"
	},
	"ok" : 1
}



//By applying the index to the field recipe in the orders collections we can notice that the speed almost doubled ! We went from 81 ms to 45 ms ! Which makes perfect sense when we see that the number of documents examined was reduced by almost 25000 ! We have proven once again that the indexing of fields in an intelligent way (in accordance to the type of searches we wish to perform over a range (Btree) or rapid succession of reads(hash) can change the performace by quite  bit)





























