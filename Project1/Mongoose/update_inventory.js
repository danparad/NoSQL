//This fun bit of code (second attempt, first is in the pizzeria file) was meant to create the server side function for updating the inventory. The idea behind this was to do a post insert operation where for each entry into the orders data base we would retrive the max order_id number (which would be the latest one to have been inserted) and find the document in the recipes collection and retrieve the ingredients. Then perform for each subdocument and update on the quantities of the corresponding ingredient in the iventories collection (which afterwards was going to check if any of the quantities were below our threshold and raise a warning and reinsert the ingredients in the inventory collection)


place_order = function(user_id, recipe_name){
var ing = orders.post('insert',
    function(orders.find({$max:{order_id}}{recipe:1}){
	recipes.find({"recipe_name":orders.find({$max:{order_id}},{recipe_name:1})},{ingredients:1}).foreach(
	function(ing){
		inventories.update({"ingredient":ing},{$inc: {"quantity":-1}
		})) 
	} 
  ) 
); 
db.system.js.save({_id: 'place_order', value: place_order})
	