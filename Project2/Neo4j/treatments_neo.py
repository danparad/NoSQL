# Author : Daniel Parada

# We import the packages we need
import random
import os

r = random.SystemRandom()

body_part = ['ankle', 'arch', 'arm', 'armpit', 'beard', 'breast', 'calf', 'cheek', 'chest', 'chin', 'earlobe', 'elbow',
             'eyebrow', 'eyelash', 'eyelid', 'face', 'finger', 'forearm', 'forehead', 'gum', 'heel', 'hip',
             'index finger', 'jaw', 'knee', 'knuckle', 'leg', 'lip', 'mouth', 'mustache', 'nail', 'neck', 'nostril',
             'palm', 'pinkie', 'pupil', 'scalp', 'shin', 'shoulder', 'sideburns', 'thigh', 'throat', 'thumb', 'tongue',
             'tooth', 'waist', 'wrist']

dr_oz_approved_treatments = ['balls', 'flying discs', 'goal posts', 'nets', 'racquets', 'rods and tackle', 'sticks',
                             'bats', 'clubs', 'slaps', 'kicks', 'headbutts', 'scratches', 'points and laughs', 'sneezes',
                             'cards against humanity', 'tables', 'phones', 'puzzles', 'cups', 'pants', 'plants', 'pools',
                             'desk corners', 'pointy things', 'sharp things', 'spoons', 'forks', 'knifes', 'cars',
                             'trucks', 'planes', 'desks', 'paper plates', 'pencils', 'iPads', 'computers', 'knowledge',
                             'hugs', 'kisses', 'finger crossing in the hopes it goes away', 'concerned looks',
                             'the power of Christ compels you', 'jock strap', 'Thanos finger snapping']

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/neo4j-community-3.4.5/import/'

# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'treatments.csv', 'w')

# We write the command to create the doctor nodes :
outputfile_1.write("treatment_id,name\n")

# We loop over 750 cases to create all of our treatments :
for i in range(1, 751):
    outputfile_1.write(str(i) + "," + str(i) + " " + dr_oz_approved_treatments[r.randint(0, 42)] + " to the "
                           + body_part[r.randint(0, 46)] + "\n")

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

with open(outputpath + 'treatments.csv', 'rb+') as filehandle:
    filehandle.seek(-1, os.SEEK_END)
    filehandle.truncate()
















