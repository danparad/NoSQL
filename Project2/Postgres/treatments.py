# Author : Daniel Parada

# We import the packages we need
import random
import os

r = random.SystemRandom()

body_part = ['ankle', 'arch', 'arm', 'armpit', 'beard', 'breast', 'calf', 'cheek', 'chest', 'chin', 'earlobe', 'elbow',
             'eyebrow', 'eyelash', 'eyelid', 'face', 'finger', 'forearm', 'forehead', 'gum', 'heel', 'hip',
             'index finger', 'jaw', 'knee', 'knuckle', 'leg', 'lip', 'mouth', 'mustache', 'nail', 'neck', 'nostril',
             'palm', 'pinkie', 'pupil', 'scalp', 'shin', 'shoulder', 'sideburns', 'thigh', 'throat', 'thumb', 'tongue',
             'tooth', 'waist', 'wrist']

dr_oz_approved_treatments = ['balls', 'flying discs', 'goal posts', 'nets', 'racquets', 'rods and tackle', 'sticks',
                             'bats', 'clubs', 'slaps', 'kicks', 'headbutts', 'scratches', 'points and laughs', 'sneezes',
                             'cards against humanity', 'tables', 'phones', 'puzzles', 'cups', 'pants', 'plants', 'pools',
                             'desk corners', 'pointy things', 'sharp things', 'spoons', 'forks', 'knifes', 'cars',
                             'trucks', 'planes', 'desks', 'paper plates', 'pencils', 'iPads', 'computers', 'knowledge',
                             'hugs', 'kisses', 'finger crossing in the hopes it goes away', 'concerned looks',
                             'the power of Christ compels you', 'jock strap', 'Thanos finger snapping']

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/NoSQL_DB/NoSQL/Project2/Postgres/'

# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'treatments.sql', 'w')

# We write the command to insert the values into the users table :
# Note : we MUST specify the fields into which we are inserting the data to make sure that it matches the schema
outputfile_1.write("INSERT INTO treatments(treatment_id, name)")
outputfile_1.write('\n')
outputfile_1.write("VALUES ")

# We loop over 750 cases to create all of our treatments :
for i in range(1, 751):
    outputfile_1.write("(" + str(i) + ", '" + str(i) + " " + dr_oz_approved_treatments[r.randint(0, 42)] + " to the "
                           + body_part[r.randint(0, 46)] + "'), ")
    outputfile_1.write('\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open('treatments.sql', 'rb+') as filehandle:
    filehandle.seek(-3, os.SEEK_END)
    filehandle.truncate()
    filehandle.write(bytes(" ; ", 'utf-8'))
    # File closes automatically

# We check that the file was correctly written
# inputfile = open(outputpath + 'test.sql', 'r')
# print(inputfile.read())


















