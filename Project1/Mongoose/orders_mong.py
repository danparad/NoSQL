# Author : Daniel Parada

# We import the packages we need
import random
import os
import itertools
import array
import time
import datetime

r = random.SystemRandom()

# We create two lists of the 100 most popular last names which will be the name of our pizzas:
l_name = ['Smith', 'Johnson', 'Williams', 'Jones', 'Brown', 'Davis', 'Miller', 'Wilson', 'Moore', 'Taylor', 'Anderson',
          'Thomas', 'Jackson', 'White', 'Harris', 'Martin', 'Thompson', 'Garcia', 'Martinez', 'Robinson', 'Clark',
          'Rodriguez', 'Lewis', 'Lee', 'Walker', 'Hall', 'Allen', 'Young']


# We declare the path where we read our input files from :
# outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/PostgresVsMongoose/Mongoose/'
outputpath = '/Users/danielparada/node_modules/mongodb/mongoose/'

# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'orders_mongo.json', 'w')


# We loop over 50000 cases to create all of our orders :
for i in range(1, 50001):
    l = l_name[r.randint(0, 27)]
    outputfile_1.write('{"order_id": ' + str(i) + ', "user_id": ' + str(r.randint(1, 1000)) + ', "time_ordered": "' +
                        str(datetime.datetime.utcnow()) + '", "recipe": "' + l +
                       '"}' + '\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()



















