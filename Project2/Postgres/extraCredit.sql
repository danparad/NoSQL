--We count the number of doctors that are treating other doctors
SELECT COUNT(DISTINCT doctor_id) FROM doc_patients WHERE sick_doc IS NOT NULL;

--We now count the number of patients that have a given illness
SELECT illness_id, count(illness_id) from illnesses_patients group by illness_id;

--We now search for the doctor that has the most patients
SELECT doctor_id, COUNT(doctor_id) FROM doc_patients GROUP BY doctor_id ORDER BY COUNT(doctor_id) LIMIT 1;

--We now search for the doctor that is treating the most amount of unique illnesses
SELECT doctor_id, COUNT(DISTINCT illness_id) FROM doc_patients JOIN illnesses_patients ON doc_patients.patient_id = illnesses_patients.patient_id GROUP BY doctor_id ORDER BY COUNT(DISTINCT illness_id) DESC LIMIT 1;

--We search for the illness that is being treated with the most unique treatments
SELECT illness_id, COUNT(DISTINCT treatment_id) FROM illnesses_patients JOIN treatments_patients ON illnesses_patients.patient_id = treatments_patients.patient_id GROUP BY illness_id ORDER BY COUNT(DISTINCT treatment_id) DESC LIMIT 1;