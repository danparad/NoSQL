SELECT * 
FROM crosstab(

'SELECT extract(year from starts) AS year, 
extract (month from starts) AS month,
extract(week from starts) AS week,
extract(DOW from starts) AS day,
count(*) 
FROM events
GROUP BY year, month, week, day',

'SELECT * 
FROM generate_series(0,6)')
AS (year int, month int,
week int,
Sunday int, Monday int, Tuesday int, Wednesday int, Thursday int, Friday int, Saturday int)
ORDER BY year,week;