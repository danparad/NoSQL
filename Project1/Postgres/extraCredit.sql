--Extra credit :





--Added the time zone constraint to the timestamp of the orders menu to reflect the Americas/Denver timezone

--ALTER TABLE orders ALTER COLUMN timestamp TYPE TIMESTAMP WITH TIME ZONE USING timestamp AT TIME ZONE 'America/Denver’;







--Added the ability to order 'different sized' (S,M,L) pizzas by adding an additional argument to the place_order function which can be a numeric value : 

--place_order(1,'Young',1) --> this only uses a quantity of 1 from each ingredient in the pizza (S)
--place_order(1,'Young',2) --> this uses a quantity of 2 from each ingredient in the pizza (M) ...





--Added the ability for different recipes to take different quantities of ingredients, i.e. cheese lovers can take multiple cheese quantities and the satanist pizza could have lots of baby tears not just 1.




--Added a function that returns a user's favorite pizza, for placing a quick order. The function takes in 1 argument, the user id, and it returns the pizza they've ordered the most