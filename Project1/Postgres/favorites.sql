CREATE OR REPLACE FUNCTION my_faves(
  usr_id INT)
  RETURNS boolean AS $$
  
  DECLARE 
    faves TEXT;
  
BEGIN


  SELECT recipe_name INTO faves
  FROM orders
  WHERE user_id = usr_id
  GROUP BY recipe_name
  ORDER BY recipe_name DESC
  LIMIT (1);
  
  RAISE NOTICE 'This appears to be your favorite : %', faves;

  RETURN TRUE;

END;

$$
LANGUAGE plpgsql;