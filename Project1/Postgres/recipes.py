# Author : Daniel Parada

# We import the packages we need
import random
import os
import itertools
import array

r = random.SystemRandom()

ingredients = ['Cheese', 'Pepperoni', 'Mushrooms', 'Baby tears', 'Oregano', 'Tomatoes', 'Onions', 'Chicken']

comb = list(itertools.combinations(ingredients, 6))
# for i in range(0, len(comb)):
#     print(i)
#     for j in range(6):
#         print(j)
#         print(comb[i][j])

sauces = ['Marinara', 'Bechamel']
description = ['Mind blowing', 'the bomb', 'mind numbingly good', 'this is one is just fine, cant win them all',
               'Pretty good', 'Awesome', 'Amazing', 'Mmmmm....', '*rubbing tummy*', 'Damn !']

# We create two lists of the 100 most popular last names which will be the name of our pizzas:
l_name = ['Smith','Johnson','Williams','Jones','Brown','Davis','Miller','Wilson','Moore','Taylor','Anderson','Thomas',
          'Jackson','White','Harris','Martin','Thompson','Garcia','Martinez','Robinson','Clark','Rodriguez','Lewis',
          'Lee','Walker','Hall','Allen','Young','Hernandez','King','Wright','Lopez','Hill','Scott','Green','Adams',
          'Baker','Gonzalez','Nelson','Carter','Mitchell','Perez','Roberts','Turner','Phillips','Campbell','Parker',
          'Evans','Edwards','Collins','Stewart','Sanchez','Morris','Rogers','Reed','Cook','Morgan','Bell','Murphy',
          'Bailey','Rivera','Cooper','Richardson','Cox','Howard','Ward','Torres','Peterson','Gray','Ramirez','James',
          'Watson','Brooks','Kelly','Sanders','Price','Bennett','Wood','Barnes','Ross','Henderson','Coleman','Jenkins',
          'Perry', 'Powell', 'Long', 'Patterson','Hughes','Flores','Washington','Butler','Simmons','Foster','Gonzales',
          'Bryant', 'Alexander', 'Russell', 'Griffin', 'Diaz', 'Hayes']


# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/PostgresVsMongoose/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'recipes.sql', 'w')

# We write the command to insert the values into the users table :
# Note : we MUST specify the fields into which we are inserting the data to make sure that it matches the schema
outputfile_1.write("INSERT INTO recipes(name, description, ingredient_1, ingredient_2, ingredient_3, ingredient_4, " +
                   "ingredient_5, ingredient_6, instructions)")
outputfile_1.write('\n')
outputfile_1.write("VALUES ")

# We loop over 25 cases to create all of our recipes :
for i in range(0, len(comb)):
    l = l_name[i]
    s = sauces[r.randint(0, 1)]
    # r = recipes[i]
    d = description[r.randint(0, 9)]
    outputfile_1.write("(" + "'" + l + "'" + ", " + "'" + str(d) + "' ")
    # for i in range(0, len(comb)):
    for j in range(6):
        outputfile_1.write(", " + "'" + comb[i][j] + "'")
    outputfile_1.write(", " + "'" + "Figure it out, I aint your ma" + "'" + ")" + ", ")
    outputfile_1.write('\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open('recipes.sql', 'rb+') as filehandle:
    filehandle.seek(-3, os.SEEK_END)
    filehandle.truncate()
    filehandle.write(bytes(" ; ", 'utf-8'))
    # File closes automatically

# We check that the file was correctly written
# inputfile = open(outputpath + 'test.sql', 'r')
# print(inputfile.read())


















