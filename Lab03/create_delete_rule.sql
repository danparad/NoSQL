CREATE RULE deletes_venues AS ON DELETE TO venues DO INSTEAD
  UPDATE venues
  SET active = 'f'
  WHERE name = name;