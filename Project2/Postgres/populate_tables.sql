--Populate doctors table:
\i doctors.sql

--Populate doctors table with sick doctors
\i sick_doctors.sql

--Populate patients table
\i patients.sql

--Populate treatments table
\i treatments.sql

--Populate illnesses table
\i illnesses.sql

--Populate doc_patients table
\i doc_patients.sql

--Populate doc_patients table with sick doctors
\i sick_doctors_patients.sql

--Populate treatments_patients table
\i treatments_patients.sql

--Populate illnesses_patients table
\i illnesses_patients.sql

--We update the doctors table to include their respective patient_ids
UPDATE doctors 
SET patient_id=subquery.patient_id 
FROM (
SELECT patient_id,sick_doc 
FROM doc_patients) AS subquery 
WHERE doctors.doctor_id = subquery.sick_doc;