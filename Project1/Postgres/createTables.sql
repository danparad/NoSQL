--Creating table Users:
CREATE TABLE Users(user_id SERIAL PRIMARY KEY, first_name VARCHAR(30) NOT NULL, last_name VARCHAR(30) NOT NULL, email VARCHAR(50), phone BIGINT, address_line1 VARCHAR(256)NOT NULL, adrress_line2 VARCHAR(256), city VARCHAR(50), state VARCHAR(50), zip INT);


--Creating table inventory
CREATE TABLE inventory(name VARCHAR(50) PRIMARY KEY, description TEXT, quantity INT DEFAULT 100);
ALTER TABLE inventory ADD CONSTRAINT check_positive CHECK (quantity > 0);
ALTER TABLE inventory DROP CONSTRAINT check_positive;


--Creating table recipes : (constraint on name of recipe must be unique)
CREATE TABLE recipes (name VARCHAR(100) PRIMARY KEY, description TEXT, ingredient_1 VARCHAR(50) NOT NULL REFERENCES inventory(name), ingredient_2 VARCHAR(50) REFERENCES inventory(name), ingredient_3 VARCHAR(50) REFERENCES inventory(name), ingredient_4 VARCHAR(50) REFERENCES inventory(name), ingredient_5 VARCHAR(50) REFERENCES inventory(name), ingredient_6 VARCHAR(50) REFERENCES inventory(name), instructions TEXT);


--Creating table ingredients_recipes
CREATE TABLE ingredients_recipes (
  recipe_name TEXT REFERENCES recipes(name),
  ingredient_name TEXT REFERENCES inventory(name),
  qty INT DEFAULT 1
);


--Creating table orders :
CREATE TABLE orders (order_id SERIAL PRIMARY KEY, user_id INT NOT NULL REFERENCES Users, timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, recipe_name VARCHAR(50) NOT NULL REFERENCES recipes, order_placed BOOLEAN DEFAULT true);

--Adding a time zone constraint to the table orders:
ALTER TABLE orders ALTER COLUMN timestamp TYPE TIMESTAMP WITH TIME ZONE USING timestamp AT TIME ZONE 'America/Denver';







