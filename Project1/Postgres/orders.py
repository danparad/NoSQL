# Author : Daniel Parada

# We import the packages we need
import random
import os
import itertools
import array

r = random.SystemRandom()

# We create two lists of the 100 most popular last names which will be the name of our pizzas:
pizza = ['Smith','Johnson','Williams','Jones','Brown','Davis','Miller','Wilson','Moore','Taylor','Anderson','Thomas',
          'Jackson','White','Harris','Martin','Thompson','Garcia','Martinez','Robinson','Clark','Rodriguez','Lewis',
          'Lee','Walker','Hall','Allen','Young']


# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/PostgresVsMongoose/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'orders.sql', 'w')

# We write the command to insert the values into the users table :
# Note : we MUST specify the fields into which we are inserting the data to make sure that it matches the schema
# outputfile_1.write("INSERT INTO orders(user_id, recipe_name)")
# outputfile_1.write('\n')
# outputfile_1.write("VALUES ")

# We loop over 25 cases to create all of our recipes :
for i in range(1, 300001):
    p = pizza[r.randint(0, 27)]
    outputfile_1.write("SELECT place_order(" + str(r.randint(1, 1000)) + ", " + "'" + p + "' " + ");" + '\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()



















