MATCH(n) OPTIONAL MATCH (n)-[r]-() DELETE n,r

//---------------------
// Creating nodes
//---------------------

//Import doctors
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/doctors.csv" AS data 
CREATE (d:Doctor {doc_id:data.doc_id, doc_name:data.name});

//Import sick doctors
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/sick_doctors.csv" AS data 
CREATE (d:Doctor:Patient {doc_id:data.doc_id, doc_name:data.name, patient_id:data.patient_id});

//Import patients
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/patients.csv" AS data1
CREATE (p:Patient {patient_id:data1.patient_id, patient_name:data1.name});

//Import illnesses
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/illnesses.csv" AS data2
CREATE (i:Illness {illness_id:data2.illness_id, illness_name:data2.name});

//Import treatments
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/treatments.csv" AS data3
CREATE (i:Treatment {treatment_id:data3.treatment_id, treatment_name:data3.name});


//---------------------
// Creating relationships
//---------------------


//Import sick doctor patients
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/sick_doc_patient.csv" AS data
MATCH(dp:Patient:Doctor {patient_id:data.patient_id}),(d:Doctor {doc_id:data.doc_id})
CREATE (d)-[:sees]->(dp);

//Import doctor_patients
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/doc_patients.csv" AS data
MATCH(p:Patient {patient_id:data.patient_id}),(d:Doctor {doc_id:data.doc_id})
CREATE (p)-[:sees]->(d);

//Import illnesses_patients
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/illnesses_patients.csv" AS data
MATCH(p:Patient {patient_id:data.patient_id}),(i:Illness {illness_id:data.illness_id})
CREATE (p)-[:has]->(i);

//Import treatments_patients
USING PERIODIC COMMIT 
LOAD CSV WITH HEADERS FROM "file:/treatments_patients.csv" AS data
MATCH(p:Patient {patient_id:data.patient_id}),(t:Treatment {treatment_id:data.treatment_id})
CREATE (p)-[:lovingly_gets]->(t);


















