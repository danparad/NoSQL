# Author : Daniel Parada
# The doctor's email address is a reference to a well known Marvin Gaye

# We import the packages we need
import random
import os

r = random.SystemRandom()

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/NoSQL_DB/NoSQL/Project2/Postgres/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'sick_doctors_patients.sql', 'w')

# We write the command to insert the values into the users table :
# Note : we MUST specify the fields into which we are inserting the data to make sure that it matches the schema
outputfile_1.write("INSERT INTO doc_patients(doctor_id, sick_doc)")
outputfile_1.write('\n')
outputfile_1.write("VALUES ")

sick_doc = range(66, 101)
k = 0
# We loop over 35 sick doctors :
for i in range(len(sick_doc)):
    healthy_docs = r.sample(range(1, 100), 5)
    for j in range(r.randint(1, 5)):
        if healthy_docs[j] != sick_doc[k]:
            outputfile_1.write("(" + str(healthy_docs[j]) + ", " + str(sick_doc[k]) + "), \n")
    k += 1

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open(outputpath + 'sick_doctors_patients.sql', 'rb+') as filehandle:
    filehandle.seek(-3, os.SEEK_END)
    filehandle.truncate()
    filehandle.write(bytes(" ; ", 'utf-8'))
    # File closes automatically

# We check that the file was correctly written
# inputfile = open(outputpath + 'test.sql', 'r')
# print(inputfile.read())


















