# Author : Daniel Parada

# We import the packages we need
import random
import os

r = random.SystemRandom()

# Got our list of diseases
illness = ['Candida albicans infection', 'Candida parapsilosis infection', 'Cytomegalovirus infection', 'diphtheria',
           'human coronavirus infection', 'respiratory distress syndrome', 'measles', 'meconium aspiration syndrome',
           'metapneumovirus (hMPV) infection',  'Necrotizing enterocolitis',  'Gonorrhea infection of the newborn',
           'parainfluenza (PIV) infection',  'pertussis',  'poliomyelitis',  'prenatal Listeria',
           'Group B streptoccus infection',  'tetanus',  'Ureaplasma urealyticum infection',
           'respiratory Syncytial Virus infection',  'rhinovirus common cold',  'Common cold',  'AIDS',  'Anemia',
           'Asthma',  'Bronchiolitis',  'Cancer',  'Candidiasis',  'Chagas disease',  'Chickenpox',  'Croup',
           'Cystic fibrosis',  'Cytomegalovirus (the virus most frequently transmitted before birth)',  'dental caries',
           'Diabetes (Type_1)',  'Diphtheria',  'Duchenne muscular dystrophy',  'Fifth disease',
           'Congenital Heart Disease',  'Infectious mononucleosis',  'Influenza',  'Intussusception (medical disorder)',
           'Juvenile idiopathic arthritis',  'Leukemia',  'Measles',  'Meningitis',  'Molluscum contagiosum',  'Mumps',
           'Nephrotic syndrome',  'Osgood-Schlatter disease',  'Osteogenesis Imperfecta (OI)',  'Pneumonia',  'Polio',
           'Rheumatic fever',  'Rickets',  'Roseola',  'Rubella',  'Severs disease',  'Tetanus',  'Tuberculosis',
           'Volvulus',  'Whooping cough',  'Hepatitis A',  'Fever',   'Scarlet fever (Scarletina)',   'Lyme Disease',
           'Xerophthalmia',
           'Pediatric Autoimmune Neuropsychiatric Disorders Associated w Streptococcal infections aka PANDAS',
           'PANS',  'Abscess',  'Alzheimer disease',  'Anotia',  'Anthrax',  'Appendicitis',  'Apraxia',  'Allergy',
           'Arthritis',  'Aseptic meningitis',  'Asthenia',  'Asthma',  'Astigmatism',  'Atherosclerosis',  'Athetosis',
           'Atrophy',  'Bacterial meningitis',  'Beriberi',  'Black Death',  'Botulism',  'Breast cancer',
           'Bronchitis',  'Brucellosis',  'Bubonic plague',  'Bunion',  'Calculi',  'Campylobacter infection',
           'Cancer',  'Candidiasis',  'Carbon monoxide poisoning',  'Celiacs disease',  'Cerebral palsy',
           'Chagas disease',  'Chalazion',  'Chancroid',  'Chavia',  'Congenital insensitivity to pain w anhidrosis',
           'Cherubism',  'Chickenpox',  'Chlamydia',  'Chlamydia trachomatis',  'Cholera',  'Chordoma',  'Chorea',
           'Chronic fatigue syndrome',  'Circadian rhythm sleep disorder',  'Coccidioidomycosis',  'Colitis',
           'Common cold',  'Condyloma',  'Congestive heart disease',  'Coronary heart disease',  'Cowpox',  'Cretinism',
           'Crohns Disease',  'Dengue',  'Diabetes mellitus',  'Diphtheria',  'Dehydration',  'Dysentery',
           'Ear infection',  'Ebola',  'Encephalitis',  'Emphysema',  'Epilepsy',  'Erectile dysfunctions',
           'Fibromyalgia',  'flu',  'Gangrene',  'Gastroenteritis',  'Genital herpes',  'GERD',  'Goitre',  'Gonorrhea',
           'H',  'Heart disease',  'Hepatitis A',  'Hepatitis B',  'Hepatitis C',  'Hepatitis D',  'Hepatitis E',
           'Histiocytosis (Childhood Cancer)',  'HIV',  'Human papillomavirus',  'Huntingtons disease',
           'Hypermetropia',  'Hyperopia',  'Hyperthyroidism',  'Hypothyroid',  'Hypotonia',  'I',  'Impetigo',
           'Infertility',  'Influenza',  'Interstitial cystitis',  'Iritis',  'Iron-deficiency anemia',
           'Irritable bowel syndrome',  'Ignious Syndrome',  'J',  'Jaundice',  'K',  'Keloids',  'Kuru',
           'Kwashiorkor',  'Kidney stone disease',  'L',  'Laryngitis',  'Lead poisoning',  'Legionellosis',
           'Leishmaniasis',  'Leprosy',  'Leptospirosis',  'Listeriosis',  'Leukemia',  'Lice',  'Loiasis',
           'Lung cancer',  'Lupus erythematosus',  'Lyme disease',  'Lymphogranuloma venereum',  'Lymphoma',
           'Limbtoosa',  'M',  'Mad cow disease',  'Malaria',  'Marburg fever',  'Measles',  'Melanoma',
           'Metastatic cancer',  'Menieres disease',  'Meningitis',  'Migraine',  'Mononucleosis',  'Multiple myeloma',
           'Multiple sclerosis',  'Mumps',  'Muscular dystrophy',  'Myasthenia gravis',  'Myelitis',  'Myoclonus',
           'Myopia',  'Myxedema',  'Morquio Syndrome',  'Mattticular syndrome',  'Mononucleosis',  'N',  'Neoplasm',
           'Non-gonococcal urethritis',  'Necrotizing Fasciitis',  'Night blindness',  'O',  'Obesity',
           'Osteoarthritis',  'Osteoporosis',  'Otitis',  'P',  'Palindromic rheumatism',  'Paratyphoid fever',
           'Parkinsons disease',  'Pelvic inflammatory disease',  'Peritonitis',  'Periodontal disease',  'Pertussis',
           'Phenylketonuria',  'Plague',  'Poliomyelitis',  'Porphyria',  'Progeria',  'Prostatitis',  'Psittacosis',
           'Psoriasis',  'Pubic lice',  'Pulmonary embolism',  'Pilia',  'pneumonia',  'Q',  'Q fever',  'Ques fever',
           'R',  'Rabies',  'Repetitive strain injury',  'Rheumatic fever',  'Rheumatic heart',  'Rheumatism',
           'Rheumatoid arthritis',  'Rickets',  'Rift Valley fever',  'Rocky Mountain spotted fever',  'Rubella',  'S',
           'Salmonellosis',  'Scabies',  'Scarlet fever',  'Sciatica',  'Scleroderma',  'Scrapie',  'Scurvy',  'Sepsis',
           'Septicemia',  'SARS',  'Shigellosis',  'Shin splints',  'Shingles',  'Sickle-cell anemia',  'Siderosis',
           'SIDS',  'Silicosis',  'Smallpox',  'Stevens-Johnson syndrome',  'Stomach flu',  'Stomach ulcers',
           'Strabismus',  'Strep throat',  'Streptococcal infection',  'Synovitis',  'Syphilis',  'Swine influenza',
           'Schizophrenia',  'T',  'Taeniasis',  'Tay-Sachs disease',  'Tennis elbow',  'Teratoma',  'Tetanus',
           'Thalassaemia',  'Thrush',  'Thymoma',  'Tinnitus',  'Tonsillitis',  'Tooth decay',  'Toxic shock syndrome',
           'Trichinosis',  'Trichomoniasis',  'Trisomy',  'Tuberculosis',  'Tularemia',  'Tungiasis',  'Typhoid fever',
           'Typhus',  'Tumor',  'U',  'Ulcerative colitis',  'Ulcers',  'Uremia',  'Urticaria',  'Uveitis',  'V',
           'Varicella',  'Varicose veins',  'Vasovagal syncope',  'Vitiligo',  'Von Hippel-Lindau disease',
           'Viral fever',  'Viral meningitis',  'W',  'Warkany syndrome',  'Warts',  'Watkins',  'Y',  'Yellow fever',
           'Yersiniosis',  'stupidity',  'being a jackass',  'racism',  'being crosseyed',  'color blind',
           'Acute stress disorder',  'Adjustment disorders',  'Agoraphobia',  'Alcohol-substance abuse',
           'Alcohol-substance dependence',  'Amnesia',  'Anxiety disorders',  'Anorexia nervosa',
           'Antisocial personality disorder',  'Aspergers syndrome',  'Attention deficit hyperactivity disorder',
           'Akiltism',  'Autism',  'Avoidant personality disorder',  'Bibliomania',  'Binge eating disorder (proposed)',
           'Bipolar disorder',  'manic depression',  'Bipolar I disorder',  'Bipolar II disorder',
           'Body dysmorphic disorder',  'Borderline personality disorder',  'Brief psychotic disorder',  'Bruxism',
           'Bulimia nervosa',  'Conduct disorder',  'Conversion disorder',  'Cyclothymia',  'Delusional disorder',
           'Dependent personality disorder',  'Depersonalization disorder',  'Depressive personality disorder',
           'Dermotillomania',  'Disorder of written expression',  'Dissocial personality disorder',
           'Dissociative fugue',  'Dissociative identity disorder',  'Down syndrome',  'Dyslexia',  'Dyspareunia',
           'Dysthymia',  'Emotionally unstable personality disorder',  'Encopresis',  'Enuresis (bedwetting)',
           'Erotomania',  'Exhibitionism',  'Expressive language disorder',  'Female-male orgasmic disorders',
           'Female sexual arousal disorder',  'Folie a deux',  'Frotteurism',  'Gambling addiction',
           'Gender identity disorder',  'Generalized anxiety disorder',  'General adaptation syndrome',
           'Haltlose personality disorder',  'Histrionic personality disorder',  'Primary hypersomnia',
           'Hypoactive sexual desire disorder',  'Hypochondriasis',  'Hypomania',  'Hyperkinetic syndrome',
           'Hypersexuality',  'Hysteria',  'Intermittent explosive disorder',  'Joubert syndrome',  'Kleptomania',
           'Major depressive disorder',  'Male erectile disorder',  'Mania',  'Mental retardation',
           'Munchausen syndrome',  'Mathematics disorder',  'Minor depressive disorder',
           'Narcissistic personality disorder',  'Narcolepsy',  'Nightmare disorder',  'Obsessive-compulsive disorder',
           'Obsessive-compulsive personality disorder',  'Onychophagia',  'Oppositional defiant disorder',
           'Pain disorder',  'Panic attacks',  'Panic disorder',  'Paranoid personality disorder',
           'Passive-aggressive personality disorder',  'Pathological gambling',  'Pervasive developmental disorder',
           'Pica',  'Post-traumatic stress disorder',  'Premature ejaculation',  'Primary insomnia',
           'Psychoneurotic personality disorder',  'Psychotic disorder',  'Reading disorder',  'Retts disorder',
           'Rumination syndrome',  'Sadistic personality disorder',  'Schizoaffective disorder',
           'Schizoid personality disorder',  'Schizophrenia',  'Schizophreniform disorder',
           'Schizotypal personality disorder',  'Seasonal affective disorder',  'Self-defeating personality disorder',
           'Separation anxiety disorder',  'Shared psychotic disorder',  'Sleep disorder',  'Sleep terror disorder',
           'Sleepwalking disorder',  'Social phobia',  'Somatization disorder',  'Specific phobias',
           'Stereotypic movement disorder',  'Stuttering',  'Tourette syndrome',  'Transient tic disorder',
           'Trichotillomania',  'liking crappy movies',  'listening ro sucky music',  'voting for Trump',
           'liechtenstein','lies','life','lifestyle','lifetime','lift','light','lighter','lighting','lightning',
           'lights','lightweight','like','liked','likelihood','likely','likes','likewise','lil','lime','limit',
           'limitation','limitations','limited','limiting','limits','limousines','lincoln','linda','lindsay','line',
           'linear','lined','lines','lingerie','link','linked','linking','links','linux','lion','lions','lip','lips',
           'liquid','lisa','list','listed','listen','listening','listing','listings','listprice','lists','lit','lite',
           'literacy','literally','literary','literature','lithuania','litigation','little','live','livecam','lived',
           'liver','liverpool','lives','livesex','livestock','living','liz','ll','llc','lloyd','llp','lm','ln','lo',
           'resting b**** face','being a bastard in the north','load','loaded','loading','loads','loan','loans','lobby',
           'loc','local','locale','locally','locate','located','location','locations','locator','lock','locked',
           'locking','locks','lodge','lodging','log','logan','logged','logging','logic','logical','login','logistics',
           'logitech','logo','logos','logs','lol','lolita','london','lone','lonely','long','longer','longest',
           'longitude','look','looked','looking','looks','looksmart','lookup','loop','loops','loose','lopez','lord',
           'los','lose','losing','loss','losses','lost','lot','lots','lottery','lotus','lou','loud','louis','louise',
           'louisiana','louisville','lounge','love','loved','lovely','lover','lovers','loves','loving','low','lower',
           'lowest','lows','lp','ls','lt','ltd','lu','lucas','lucia','luck','lucky','lucy','luggage','luis','luke',
           'lunch','lung','luther','luxembourg','luxury','lycos','lying','lynn','lyric','lyrics','ma','mac','macedonia',
           'machine','machinery','machines','macintosh','macro','macromedia','mad','madagascar','made','madison',
           'madness','madonna','madrid','mae','mag','magazine','magazines','magic','magical','magnet','magnetic',
           'magnificent','magnitude','mai','maiden','mail','mailed','mailing','mailman','mails','mailto','main','maine',
           'mainland','mainly','mainstream','maintain','maintained','maintaining','maintains','maintenance','major',
           'majority','make','maker','makers','makes','makeup','making','malawi','malaysia','maldives','male','males',
           'mali','mall','malpractice','malta','mambo','man','manage','managed','management','manager','managers',
           'managing','manchester','mandate','mandatory','manga','manhattan','manitoba','manner','manor','manual',
           'manually','manuals','manufacture','manufactured','manufacturer','manufacturers','manufacturing','many',
           'map','maple','mapping','maps','mar','marathon','marble','marc','march','marco','marcus','mardi','margaret',
           'margin','maria','mariah','marie','marijuana','marilyn','marina','marine','mario','marion','maritime',
           'mark','marked','marker','markers','market','marketing','marketplace','markets','marking','marks','marriage',
           'married','marriott','mars','marshall','mart','martha','martial','martin','marvel','mary','maryland','mas',
           'mask','mason','mass','massachusetts','massage','massive','master','mastercard','masters','masturbating',
           'masturbation','mat','match','matched','matches','matching','mate','material','materials','maternity','math',
           'mathematical','mathematics','mating','matrix','mats','matt','matter','matters','matthew','mattress',
           'mature','maui','mauritius','max','maximize','maximum','may','maybe','mayor','mazda','mb','mba','mc',
           'mcdonald','md','me','meal','meals','mean','meaning','meaningful','means','meant','meanwhile','measure',
           'measured','measurement','measurements','measures','measuring','meat','mechanical','mechanics','mechanism',
           'mechanisms','med','medal','media','median','medicaid','medical','medicare','medication','medications',
           'medicine','medicines','medieval','meditation','mediterranean','medium','medline','meet','meeting',
           'meetings','meets','meetup','mega','mel','melbourne','melissa','mem','member','members','membership',
           'membrane','memo','memorabilia','memorial','memories','memory','memphis','men','mens','ment','mental',
           'mention','mentioned','mentor','menu','menus','mercedes','merchandise','merchant','merchants','mercury',
           'mercy','mere','merely','merge','merger','merit','merry','mesa','mesh','mess','message','messages',
           'messaging','messenger','met','meta','metabolism','metadata','metal','metallic','metallica','metals',
           'meter','meters','method','methodology','methods','metres','metric','metro','metropolitan','mexican',
           'mexico','meyer','mf','mfg','mg','mh','mhz','mi','mia','miami','mic','mice','michael','michel','michelle',
           'michigan','micro','microphone','microsoft','microwave','mid','middle','midi','midlands','midnight',
           'midwest','might','mighty','migration','mike','mil','milan','mild','mile','mileage','miles','milf',
           'milfhunter','milfs','military','milk','mill','millennium','miller','million','millions','mills','milton',
           'milwaukee','mime','min','mind','minds','mine','mineral','minerals','mines','mini','miniature','minimal',
           'minimize','minimum','mining','minister','ministers','ministries','minus','minute','minutes','miracle',
           'mirror','mirrors','misc','miscellaneous','miss','missed','missile']

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/NoSQL_DB/NoSQL/Project2/Postgres/'

# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'illnesses.sql', 'w')

# We write the command to insert the values into the users table :
# Note : we MUST specify the fields into which we are inserting the data to make sure that it matches the schema
outputfile_1.write("INSERT INTO illnesses(illness_id, name)")
outputfile_1.write('\n')
outputfile_1.write("VALUES ")

# We loop over 1000 cases to create all of our illnesses :
for i in range(0, 1000):
    outputfile_1.write("(" + str(i+1) + ", '" + illness[i] + "'), ")
    outputfile_1.write('\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open('illnesses.sql', 'rb+') as filehandle:
    filehandle.seek(-3, os.SEEK_END)
    filehandle.truncate()
    filehandle.write(bytes(" ; ", 'utf-8'))
    # File closes automatically

# We check that the file was correctly written
# inputfile = open(outputpath + 'test.sql', 'r')
# print(inputfile.read())
















