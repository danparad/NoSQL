map = function(){
	var longitudes = Math.trunc(this.location.longitude);
		emit({
			longs:longitudes
		},{
			count:1
		});
	}
	
reduce = function(key,values){
	var total=0;
	for(var i=0; i<values.length; i++){
		total += values[i].count;
	}
	return{count:total};
}

result=db.runCommand({mapReduce:'cities',map:map,reduce:reduce,out:'cities_longitude'})