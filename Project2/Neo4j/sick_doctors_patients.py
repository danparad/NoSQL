# Author : Daniel Parada
# The doctor's email address is a reference to a well known Marvin Gaye

# We import the packages we need
import random
import os

r = random.SystemRandom()

# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/neo4j-community-3.4.5/import/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'sick_doc_patient.csv', 'w')

# We write the command to create the doctor nodes :
outputfile_1.write("doc_id,patient_id,sick_doc_id\n")

# sick_doc = r.sample(range(1, 100), 35)
sick_doc = range(66, 101)
# We loop over 35 sick doctors :
k = 0
l = 66
for i in range(9966, 10001):
    healthy_docs = r.sample(range(1, 100), 5)
    for j in range(r.randint(1, 5)):
        if healthy_docs[j] != sick_doc[k]:
            outputfile_1.write(str(healthy_docs[j]) + "," + str(i) + "," + str(l) + "\n")
    k += 1
    l += 1

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open(outputpath + 'sick_doc_patient.csv', 'rb+') as filehandle:
    filehandle.seek(-1, os.SEEK_END)
    filehandle.truncate()


















