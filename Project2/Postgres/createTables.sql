--Creating table Doctors:
CREATE TABLE Doctors(doctor_id SERIAL PRIMARY KEY, first_name VARCHAR(30) NOT NULL, last_name VARCHAR(30) NOT NULL, email VARCHAR(50), phone char(12), hospital_address VARCHAR(256), patient_id INT DEFAULT NULL);

--Creating the table Patients:
CREATE TABLE Patients(patient_id SERIAL PRIMARY KEY, first_name VARCHAR(30) NOT NULL, last_name VARCHAR(30) NOT NULL, email VARCHAR(50), phone char(12), address VARCHAR(256));

--Creating the table Treatments:
CREATE TABLE Treatments(treatment_id SERIAL PRIMARY KEY, name VARCHAR(200));

--Creating the table Illnesses:
CREATE TABLE Illnesses(illness_id SERIAL PRIMARY KEY, name VARCHAR(200));

--Creating the table doc_patients:
CREATE TABLE doc_patients(doctor_id INT REFERENCES doctors(doctor_id), patient_id SERIAL REFERENCES Patients(patient_id), sick_doc INT REFERENCES doctors(doctor_id) DEFAULT NULL);

--Creating the table treatments_patients:
CREATE TABLE treatments_patients(treatment_id INT REFERENCES treatments(treatment_id), patient_id INT REFERENCES Patients(patient_id));

--Creating the table illnesses_patients:
CREATE TABLE illnesses_patients(illness_id INT REFERENCES illnesses(illness_id), patient_id INT REFERENCES Patients(patient_id));