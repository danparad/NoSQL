//Commands to populate tables :

mongoimport --db Pizzeria --collection users --file users_mongo.json

mongoimport --db Pizzeria --collection inventories --file ingredients_mongo.json

mongoimport --db Pizzeria --collection recipes --file recipes_mongo.json

mongoimport --db Pizzeria --collection orders --file orders_mongo.json
