# Author : Daniel Parada

# We import the packages we need
import random
import os
import itertools
import array

r = random.SystemRandom()

ingredients = ['Cheese', 'Pepperoni', 'Mushrooms', 'Baby tears', 'Oregano', 'Tomatoes', 'Onions', 'Chicken']

comb = list(itertools.combinations(ingredients, 6))

sauces = ['Marinara', 'Bechamel']
description = ['Mind blowing', 'the bomb', 'mind numbingly good', 'this is one is just fine, cant win them all',
               'Pretty good', 'Awesome', 'Amazing', 'Mmmmm....', '*rubbing tummy*', 'Damn !']

# We create two lists of the 100 most popular last names which will be the name of our pizzas:
l_name = ['Smith','Johnson','Williams','Jones','Brown','Davis','Miller','Wilson','Moore','Taylor','Anderson','Thomas',
          'Jackson','White','Harris','Martin','Thompson','Garcia','Martinez','Robinson','Clark','Rodriguez','Lewis',
          'Lee','Walker','Hall','Allen','Young','Hernandez','King','Wright','Lopez','Hill','Scott','Green','Adams',
          'Baker','Gonzalez','Nelson','Carter','Mitchell','Perez','Roberts','Turner','Phillips','Campbell','Parker',
          'Evans','Edwards','Collins','Stewart','Sanchez','Morris','Rogers','Reed','Cook','Morgan','Bell','Murphy',
          'Bailey','Rivera','Cooper','Richardson','Cox','Howard','Ward','Torres','Peterson','Gray','Ramirez','James',
          'Watson','Brooks','Kelly','Sanders','Price','Bennett','Wood','Barnes','Ross','Henderson','Coleman','Jenkins',
          'Perry', 'Powell', 'Long', 'Patterson','Hughes','Flores','Washington','Butler','Simmons','Foster','Gonzales',
          'Bryant', 'Alexander', 'Russell', 'Griffin', 'Diaz', 'Hayes']


# We declare the path where we read our input files from :
# outputpath = '/Users/danielparada/Desktop/MS_DS/Summer_Quarter/NoSQL/PostgresVsMongoose/Mongoose/'
outputpath = '/Users/danielparada/node_modules/mongodb/mongoose/'

# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'recipes_mongo.json', 'w')

# We loop over 25 cases to create all of our recipes :
for i in range(0, len(comb)):
    l = l_name[i]
    d = description[r.randint(0, 9)]
    outputfile_1.write('{"name": "' + l + '", "description": "' + str(d) + '", "ingredients": {')
    for j in range(5):
        outputfile_1.write('"ingredient_' + str(j+1) + '": "' + comb[i][j] + '", ')
    outputfile_1.write('"ingredient_6": "' + comb[27][5] + '"}, "instructions": "Figure it out, I aint your ma"}')
    outputfile_1.write('\n')

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()


















