# Author : Daniel Parada
# The doctor's email address is a reference to a well known Marvin Gaye

# We import the packages we need
import random
import os

r = random.SystemRandom()

#We create two lists of the 100 most popular names and last names :
f_name = ['Sophia','Olivia','Emma','Ava','Isabella','Mia','Aria','Riley','Zoe','Amelia','Layla','Charlotte','Aubrey',
          'Lily','Chloe','Harper','Evelyn','Adalyn','Emily','Abigail','Madison','Aaliyah','Avery','Ella','Scarlett',
          'Maya','Mila','Nora','Camilla','Arianna','Eliana','Hannah','Leah','Ellie','Kaylee','Kinsley','Hailey','Madelyn',
          'Paisley','Elizabeth','Addison','Isabelle','Anna','Sarah','Brooklyn','Mackenzie','Victoria','Luna','Penelope',
          'Grace','Jackson','Liam','Noah','Aiden','Lucas','Caden','Grayson','Mason','Elijah','Logan','Oliver','Ethan',
          'Jayden','Muhammad','Carter','Michael','Sebastian','Alexander','Jacob','Benjamin','James','Ryan','Matthew',
          'Daniel','Jayce','Mateo','Caleb','Luke','Julian','Jack','William','Wyatt','Gabriel','Connor','Henry','Isaiah',
          'Isaac','Owen','Levi','Cameron','Nicholas','Josiah','Lincoln','Dylan','Samuel','John','Nathan','Leo','David',
          'Adam']

l_name = ['Smith','Johnson','Williams','Jones','Brown','Davis','Miller','Wilson','Moore','Taylor','Anderson','Thomas',
          'Jackson','White','Harris','Martin','Thompson','Garcia','Martinez','Robinson','Clark','Rodriguez','Lewis',
          'Lee','Walker','Hall','Allen','Young','Hernandez','King','Wright','Lopez','Hill','Scott','Green','Adams',
          'Baker','Gonzalez','Nelson','Carter','Mitchell','Perez','Roberts','Turner','Phillips','Campbell','Parker',
          'Evans','Edwards','Collins','Stewart','Sanchez','Morris','Rogers','Reed','Cook','Morgan','Bell','Murphy',
          'Bailey','Rivera','Cooper','Richardson','Cox','Howard','Ward','Torres','Peterson','Gray','Ramirez','James',
          'Watson','Brooks','Kelly','Sanders','Price','Bennett','Wood','Barnes','Ross','Henderson','Coleman','Jenkins',
          'Perry','Powell','Long','Patterson','Hughes','Flores','Washington','Butler','Simmons','Foster','Gonzales',
          'Bryant','Alexander','Russell','Griffin','Diaz','Hayes']

streets = ['street', 'avenue', 'court', 'boulevard', 'circle', 'way']


# We define a function that will create random phone numbers :
def phn():
    n = '0000000000'
    while '9' in n[3:6] or n[3:6] == '000' or n[6] == n[7] == n[8] == n[9]:
        n = str(random.randint(10**9, 10**10-1))
    return n[:3] + '-' + n[3:6] + '-' + n[6:]


def address():
    return str(r.randint(1, 3000)) + " " + l_name[r.randint(0, 99)] + " " + streets[r.randint(0, 5)]


# We declare the path where we read our input files from :
outputpath = '/Users/danielparada/Desktop/neo4j-community-3.4.5/import/'


# We give the path and name of our output file where we are going to create our csv file with our Representatives
outputfile_1 = open(outputpath + 'sick_doctors.csv', 'w')

# We write the command to create the doctor nodes :
outputfile_1.write("doc_id,name,patient_id\n")

# We loop over 100, to create the doctors :
j = 9966
for i in range(66, 101):
    f = f_name[r.randint(0, 99)]
    l = l_name[r.randint(0, 99)]
    outputfile_1.write(str(i) + "," + f + " " + l + "," + str(j) + "\n")
    j += 1

# We close our input/output file in order to avoid memory leaks and prevent reading/writing errors
outputfile_1.close()

# We reopen the file (in binary format, i.e. 'rb+') and truncate the last 3 characters so we change the comma for a
# semi-colon at the end. This is why we need to cast the semi-colon symbol to a bytes to write it. This fully automates
# the creation of the user data file
with open(outputpath + 'sick_doctors.csv', 'rb+') as filehandle:
    filehandle.seek(-1, os.SEEK_END)
    filehandle.truncate()


















